//
//  InfoItemViewController.swift
//  TODO
//
//  Created by USRDEL on 15/5/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class InfoItemViewController: UIViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //Desde la vista de la tabla, se captura y se saca el item. El item y el índice vienen desde la tabla
    var itemInfo:(itemManager: ItemManager, index: Int)?
    
    // copia del Item. Utiliza más memoria.
    //var item:Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title
        
        locationLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location
        
        descriptionLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].itemDescription

    }
    
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        
        navigationController?.popViewController(animated: true)
    }
    
}
