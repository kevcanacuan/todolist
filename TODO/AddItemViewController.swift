//
//  AddItemViewController.swift
//  TODO
//
//  Created by USRDEL on 9/5/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {
    
    @IBOutlet weak var TittleTextField: UITextField!
    @IBOutlet weak var LocationTextField: UITextField!
    @IBOutlet weak var DescriptionTextField: UITextField!
    
    var itemManager:ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func CancelButtonPressed(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
        
        let itemTittle = TittleTextField.text ?? ""
        let itemLocation = LocationTextField.text ?? ""
        let itemDescription = DescriptionTextField.text ?? ""
        
        
        /* let item = Item(
            tittle: itemTittle,
            location: itemLocation,
            description: itemDescription)
 
        */
        
        //if (itemTittle == ""){
           // print ("Titulo vacio")
        //}
        //if (itemDescription == ""){
          //  print ("Descripción vacía")
        //}
        
        
        //Cambios para Realm
        
        
        
        if itemTittle == "" || itemLocation == "" || itemDescription == ""{
            showAlert(title: "Error", message: "All items are required")
        }
        
        else {
            
            //itemManager?.toDoItems += [item]
            //Guardar en la Base de Datos y ya no en el arreglo
            itemManager?.addItem(title: itemTittle, location: itemLocation, itemDescription: itemDescription)
            
        }
        
        //método para cerrar la ventana cuando se crea un item
        
        navigationController?.popViewController(animated: true)
        
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction (title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}
