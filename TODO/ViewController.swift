//
//  ViewController.swift
//  TODO
//
//  Created by USRDEL on 9/5/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let itemManager = ItemManager()

    @IBOutlet weak var itemsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let item1 = Item(tittle: "ToDo 1", location: "Office", description: "Do something")
        //let item2 = Item(tittle: "ToDo 2", location: "House", description: "Do something else")
        //let item3 = Item(tittle: "ToDo 3", location: "Uni", description: "Study ?")
        
        //itemManager.toDoItems = [item1, item2, item3]
        
        //let item4 = Item(tittle: "Done 1", location: "Narnia", description: "bla bla bla")
        //let item5 = Item(tittle: "Done 2", location: "House", description: "Read Clean Code :)")
        
        //itemManager.doneItems = [item4, item5]
    }
    
    //Funcion que se ejecuta cada vez que la vista aparece
    override func viewWillAppear(_ animated: Bool) {
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue" {
            
            let destination = segue.destination as! AddItemViewController
            destination.itemManager = itemManager
            
        }
        
        if segue.identifier == "toItemInfoSegue" {
            
            let destination = segue.destination as! InfoItemViewController
            
            //arreglo de todas las reglas seleccionadas
            let selectRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectRow.row)
        }
    
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //celda vacía
        //let cell = UITableViewCell()
        
        //celda desde la tabla
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].location
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].location
        }
        else {
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].location
            cell.locationLabel.text = ""
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            performSegue(withIdentifier: "toItemInfoSegue", sender: self)
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
    
    //Funcuión para realizar cambios de pantallas al deslizarse sobre la opción
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    
    //Secciones editables
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            itemManager.checkItem(index: indexPath.row)
        } else {
            itemManager.unCheckItem(index: indexPath.row)
        }
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }

}

