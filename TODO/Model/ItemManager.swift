//
//  ItemManager.swift
//  TODO
//
//  Created by USRDEL on 9/5/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
    
    var toDoItems:[Item] = []
    var doneItems:[Item] = []
    
    //Instancia de Realm --> se va a compartir con toda la aplicación
    var realm:Realm
    init() {
        realm = try! Realm()
        print (realm.configuration.fileURL)
    }
    
    func checkItem(index:Int){
        //let item = toDoItems.remove(at: index)
        //doneItems += [item]
        
        //Con Realm
        let item = toDoItems[index]
        
        try! realm.write {
            item.done = true
        }
    }
    
    //Move item from "done" to "toDo"
    func unCheckItem(index:Int){
        //let item = doneItems.remove(at: index)
        //toDoItems += [item]
        
        //Con Realm
        let item = doneItems[index]
        
        try! realm.write {
            item.done = false
        }
    }
    
    
    func addItem(title:String, location:String, itemDescription:String?) {
        
        let item = Item()
        item.id = "\(UUID())"
        item.title = title
        item.location = location
        item.itemDescription = itemDescription
        
        //Guardar en la Base
        try! realm.write {
            realm.add(item)
        }
        
        //getTodoItems()
        
        
    }
    
    func updateArrays() {
        toDoItems = Array(realm.objects(Item.self).filter("done = false"))
        doneItems = Array(realm.objects(Item.self).filter("done = true"))
    }
    
}
