//
//  Item.swift
//  TODO
//
//  Created by USRDEL on 9/5/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    
    @objc dynamic var id:String?
    @objc dynamic var title:String?
    @objc dynamic var location:String?
    @objc dynamic var itemDescription:String?
    @objc dynamic var done = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


/* struct Item {
    let tittle:String
    let location:String
    let description:String
    
    
} */


//Realm es una base no relacional, pero si se puede crear relaciones que serán manejadas por el Software mismo.
